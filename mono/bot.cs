using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot {
	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, new Join(botName, botKey));
		}
	}

	private StreamWriter writer;
    CarID car = new CarID();
    double throttle = 1.0;
    double oldAngle = 0.0;

	Bot(StreamReader reader, StreamWriter writer, Join join) {
		this.writer = writer;
		string line;

		send(join);

		while((line = reader.ReadLine()) != null) {
			InMsgWrapper msg = JsonConvert.DeserializeObject<InMsgWrapper>(line);
			switch(msg.msgType) {
				case "carPositions":
                    throttle = getThrottle(msg.data);
					send(new Throttle(throttle));
					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					Console.WriteLine("Race init");
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
                case "yourCar":
                    car = getCar(msg.data);
                    break;
				default:
					send(new Ping());
					break;
			}
		}
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}

    private CarID getCar(object data)
    {
        JObject objectData = JObject.Parse(data.ToString());
        CarID c = new CarID();
        c.name = (string)objectData["name"];
        c.color = (string)objectData["color"];
        return c;
    }

    private double getThrottle(object data) {
        JArray objectData = JArray.Parse(data.ToString());
        foreach (var item in objectData.Children())
        {
            if ((string)item["id"]["color"] == car.color)
            {
                if (Math.Abs((double)item["angle"]) > oldAngle)
                {
                    oldAngle = Math.Abs((double)item["angle"]);
                    return Math.Max(throttle - 0.1, 0.0);
                }
                oldAngle = Math.Abs((double)item["angle"]);
                return Math.Min(throttle + 0.1, 1.0);
            }
        }
        return 1.0;
    }



}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
        this.data = data;
    }
}

class InMsgWrapper
{
    public string msgType;
    public Object data;

    public InMsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
        this.data = data;
    }
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class Race
{
    public Track track { get; set; }
    public Car[] cars { get; set; }
    public RaceSession raceSession { get; set; }
}

class Track
{
    public string id { get; set; }
    public string name { get; set; }
    public TrackPiece[] pieces  { get; set; }
    public TrackLane[] lanes { get; set; }
    public StartingPoint startingPoint { get; set; }
}

class TrackPiece
{
    public double length { get; set; }
    public double radius { get; set; }
    public double angle { get; set; }
    public bool Switch { get; set; }
}

class TrackLane
{
    public int distancefromCenter { get; set; }
    public int index { get; set; }
}

class StartingPoint
{
    public Point position { get; set; }
    public double angle { get; set; }
}

class Point
{
    public double x { get; set; }
    public double y { get; set; }
}

class Car
{
    public CarID id { get; set; }
    public CarDimensions dimensions { get; set; }
}

class CarID
{
    public string name { get; set; }
    public string color { get; set; }
}

class CarDimensions
{
    public double length { get; set; }
    public double width { get; set; }
    public double guideFlagPosition { get; set; }
}

class RaceSession
{
    public int laps { get; set; }
    public int maxLapTimeMs { get; set; }
    public bool quickRace { get; set; }
}